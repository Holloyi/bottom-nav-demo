package com.holloyi.bottomnavdemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import android.graphics.Color;
import android.os.Bundle;

import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.bottomnavigation.BottomNavigationView;

@SuppressWarnings("all")
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        NavController navController = Navigation.findNavController(this,R.id.nav_host);
        BottomNavigationView navigationView = findViewById(R.id.bottom_nav_view);
        NavigationUI.setupWithNavController(navigationView,navController);
        //获取角标
        BadgeDrawable badge1 = navigationView.getOrCreateBadge(R.id.firstFragment);
        BadgeDrawable badge2 = navigationView.getOrCreateBadge(R.id.secondFragment);
        BadgeDrawable badge3 = navigationView.getOrCreateBadge(R.id.thirdFragment);
        //角标是否显示
        badge1.setVisible(true);
        //角标位置
        badge1.setBadgeGravity(BadgeDrawable.TOP_START);
        //显示数量
        badge2.setNumber(10);
        badge3.setNumber(99);
        //角标背景
        badge3.setBackgroundColor(R.color.black);
        badge3.setBadgeGravity(BadgeDrawable.TOP_END);


    }
}